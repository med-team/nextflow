Source: nextflow
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Steffen Moeller <moeller@debian.org>,
           Pierre Gruet <pgt@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               default-jdk-headless,
               javahelper,
               gradle-debian-helper,
               maven-repo-helper,
               capsule-nextflow,
               libcommons-codec-java,
               libfailsafe-java,
               libgoogle-auth-java,
               libgpars-groovy-java,
               libgrengine-java,
               libjackson2-databind-java,
               libjackson2-dataformat-yaml,
               libjline2-java,
               libjoda-time-java,
               libkryo-java,
               libleveldb-java,
               liblog4j1.2-java,
               libmail-java,
               libobjenesis-java,
               libokhttp-java,
               libpf4j-java,
               libpf4j-update-java,
               libswagger-core-java,
               libyaml-snake-java,
               junit4 <!nocheck>
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/med-team/nextflow
Vcs-Git: https://salsa.debian.org/med-team/nextflow.git
Homepage: https://github.com/nextflow-io/nextflow
Rules-Requires-Root: no

Package: nextflow
Architecture: all
Depends: capsule-nextflow,
         libcapsule-maven-nextflow-java,
         ${java:Depends},
         ${misc:Depends}
Description: DSL for data-driven computational pipelines
 Nextflow is a bioinformatics workflow manager that enables the
 development of portable and reproducible workflows. It supports
 deploying workflows on a variety of execution platforms including local,
 HPC schedulers, AWS Batch, Google Genomics Pipelines, and Kubernetes.
 Additionally, it provides support for manage your workflow dependencies
 through built-in support for Conda, Docker, Singularity, and Modules.
